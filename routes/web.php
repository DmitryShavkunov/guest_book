<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
//profile
Route::get('/profile/', 'ProfileController@index')->name('profile');
Route::post('/profile/add_pic.php', 'ProfileController@addPic')->name('addPic');

//records
Route::get('/guest_book', 'RecordController@index')->name('guest_book');
Route::post('/guest_book/send_message.php', 'RecordController@store')->name('send_message');
Route::post('/guest_book/edit_message.php', 'RecordController@edit')->name('edit_message');
Route::put('/guest_book/update_message.php', 'RecordController@update')->name('update_message');
Route::delete('/guest_book/delete_message.php', 'RecordController@destroy')->name('delete_message');

//admin
Route::get('/admin', 'AdminController@index')->name('admin');
Route::post('/admin/add_stop_word.php', 'AdminController@addStopWord')->name('add_stop_word');
Route::delete('/admin/deletestop_word.php', 'AdminController@destroy')->name('deletestop_word');;