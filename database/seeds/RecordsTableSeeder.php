<?php

use Illuminate\Database\Seeder;

class RecordsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('records')->delete();

        DB::table('records')->insert(['user_id'=>'1',
            'message' => 'Здравствуйте, я гений и по совместительству администратор данного сайта. Здесь вы можете оставить отзыв о нашем сайте. Будте внимательны, на сайте работает фильтр стоп слов, который превратит ваши сообщения в набор символов.',
            'created_at'=>'2018-08-14 17:38:39','updated_at'=>'2016-08-01 17:38:39']);


        DB::table('records')->insert(['user_id'=>'2',
            'message' => 'Эшкере',
            'created_at'=>'2016-10-01 17:38:39','updated_at'=>'2016-10-01 17:38:39']);
        DB::table('records')->insert(['user_id'=>'2',
            'message' => 'Ваш сервич зашквар. Как так то',
            'created_at'=>'2016-11-02 17:38:39','updated_at'=>'2016-11-02 17:38:39']);
        DB::table('records')->insert(['user_id'=>'2',
            'message' => 'Не смотрите сюда',
            'created_at'=>'2016-11-03 17:38:39','updated_at'=>'2016-11-03 17:38:39']);
        DB::table('records')->insert(['user_id'=>'3',
            'message' => 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.',
            'created_at'=>'2016-11-04 17:38:39','updated_at'=>'2016-11-04 17:38:39']);
        DB::table('records')->insert(['user_id'=>'3',
            'message' => 'Lorem ipsum dolor sit amet, consectetur adipiscing elit.',
            'created_at'=>'2018-08-01 17:38:39','updated_at'=>'2018-08-01 17:38:39']);
        DB::table('records')->insert(['user_id'=>'2',
            'message' => 'В западной традиции рыбой выступает фрагмент латинского текста из философского трактата Цицерона «О пределах добра и зла», написанного в 45 году до нашей эры. Впервые этот текст был применен для набора шрифтовых образцов неизвестным печатником еще в XVI веке.',
            'created_at'=>'2018-08-05 17:38:39','updated_at'=>'2018-08-05 17:38:39']);
        DB::table('records')->insert(['user_id'=>'3',
            'message' => 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.',
            'created_at'=>'2018-08-06 17:38:39','updated_at'=>'2018-08-06 17:38:39']);
        DB::table('records')->insert(['user_id'=>'1',
            'message' => 'Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo.',
            'created_at'=>'2018-08-07 17:38:39','updated_at'=>'2018-08-07 17:38:39']);
        DB::table('records')->insert(['user_id'=>'2',
            'message' => 'аааааааааа ыфыыыыыыыыыы цуууууууууууууууу цццццццццццц йййййййййййййййй',
            'created_at'=>'2018-08-08 17:38:39','updated_at'=>'2018-08-08 17:38:39']);
        DB::table('records')->insert(['user_id'=>'2',
            'message' => 'навэльный. Все на митинг',
            'created_at'=>'2018-08-09 17:38:39','updated_at'=>'2018-08-09 17:38:39']);
        DB::table('records')->insert(['user_id'=>'3',
            'message' => 'Что происходит?',
            'created_at'=>'2018-08-10 17:38:39','updated_at'=>'2018-08-10 17:38:39']);
    }
}
