<?php

use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->delete();

        $password = Hash::make("qwerty123");
        DB::table('users')->insert(['id' => '1', 'name'=>'Hideo Kojima', 'email' => 'kojima@geniy.com', 'password' => $password, 'picture' => 'https://www.kinonews.ru/insimgs/2017/newsimg/newsimg73931.jpg',
            'created_at'=>'2018-08-14 17:38:39','updated_at'=>'2018-08-01 17:38:39', 'role' => 'admin']);
        DB::table('users')->insert(['id' => '2', 'name'=>'No name', 'email' => 'noname@mail.ru', 'password' => $password,
            'created_at'=>'2018-08-14 17:38:39','updated_at'=>'2018-08-01 17:38:39']);
        DB::table('users')->insert(['id' => '3', 'name'=>'Бабулита', 'email' => 'babulita@mail.ru', 'password' => $password, 'picture' => 'http://memesmix.net/media/created/dbtg37.jpg',
            'created_at'=>'2018-08-14 17:38:39','updated_at'=>'2018-08-01 17:38:39']);
    }
}
