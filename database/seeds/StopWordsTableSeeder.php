<?php

use Illuminate\Database\Seeder;
use App\StopWord;

class StopWordsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('stop_words')->delete();

        DB::table('stop_words')->insert(['created_at'=>'2018-08-14 17:38:39','updated_at'=>'2018-08-01 17:38:39','word' => 'зашквар']);
        DB::table('stop_words')->insert(['created_at'=>'2018-08-14 17:38:39','updated_at'=>'2018-08-02 17:38:39','word' => 'лох']);
        DB::table('stop_words')->insert(['created_at'=>'2018-08-14 17:38:39','updated_at'=>'2018-08-03 17:38:39','word' => 'блэт']);
        DB::table('stop_words')->insert(['created_at'=>'2018-08-14 17:38:39','updated_at'=>'2018-08-04 17:38:39','word' => 'навэльный']);
        DB::table('stop_words')->insert(['created_at'=>'2018-08-14 17:38:39','updated_at'=>'2018-08-05 17:38:39','word' => 'суицид']);
        DB::table('stop_words')->insert(['created_at'=>'2018-08-14 17:38:39','updated_at'=>'2018-08-06 17:38:39','word' => 'чухан']);

    }
}
