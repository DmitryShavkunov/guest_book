<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Record extends Model
{
    //
    public $f_message;//сообщение после фильтра
    public $user_name;
    public $user_pic;

    public function create( $message, $user_id )
    {
        $this->message = $message;
        $this->user_id = $user_id;
        $this->save();
    }
    public function users()//1:M
    {
        return $this->belongsTo('App\User', 'user_id','id');
    }
}
