<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Record;
use App\StopWord;
use App\User;

class AdminController extends Controller
{
    public function __construct()//доступ только для авторизованных юзеров
    {
        $this->middleware('auth');
        $this->middleware('admin');
    }

    public function index()
    {
        //
        $records = Record::orderBy('created_at')->paginate(10);
        $stop_words = StopWord::all();
        foreach($records as $record) {
            $record->f_message = RecordController::stopWordsFilter($record->message);
            $record->user_name = User::all()->find($record->user_id)->name;
            $record->user_pic = User::all()->find($record->user_id)->picture;
            $record->save();
        }
        return view('admin.index', ['records' =>$records, 'words' => $stop_words]);
    }

    public function addStopWord(Request $request)
    {
        //
        $new_word = new StopWord;
        $new_word->word=$request->input('word');
        $new_word->save();
        return redirect('/admin');
    }

    public function destroy(Request $request)
    {
        $word = StopWord::all()->find($request->input('id'));
        $word->delete();
        return redirect('/admin');
    }
}
