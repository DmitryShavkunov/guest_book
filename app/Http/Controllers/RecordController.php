<?php

namespace App\Http\Controllers;

use App\Record;
use App\StopWord;
use Illuminate\Http\Request;
use Auth;
use App\User;

class RecordController extends Controller
{
    public function __construct()//доступ только для авторизованных юзеров
    {
        $this->middleware('auth');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $records = Record::orderBy('created_at','desc')->paginate(10);

        //реализация фильтра стоп слов
        foreach($records as $record) {
            $record->f_message = RecordController::stopWordsFilter($record->message);
            $record->user_name = User::all()->find($record->user_id)->name;
            $record->user_pic = User::all()->find($record->user_id)->picture;
            $record->save();
        }

        return view('records.index', ['records' =>$records, 'user' => Auth::user()]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $new_record = new Record;
        $new_record->create($request->input('message'), Auth::user()->id);
        return redirect('/guest_book');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Record  $record
     * @return \Illuminate\Http\Response
     */
    public function show(Record $record)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Record  $record
     * @return \Illuminate\Http\Response
     */
    public function edit(Request $request)
    {
        $record = Record::find($request->input('id'));
        $isAdmin=$request->input('fromAdminPage');//если удаляется из админки редиректит туда же
        return view('records.update', ['record' =>$record, 'isAdmin' => $isAdmin]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Record  $record
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        $record = Record::find($request->input('id'));
        $record->message = $request->input('message');
        $record->save();

        if($request->input('fromAdminPage'))//если удаляется из админки редиректит туда же
            return redirect('/admin');
        return redirect('/guest_book');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Record  $record
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request)
    {
        $record = Record::all()->find($request->input('id'));
        $record->delete();

        if($request->input('fromAdminPage'))//если удаляется из админки редиректит туда же
            return redirect('/admin');
        return redirect('/guest_book');
    }


    public static function stopWordsFilter($sended_message)
    {
        $message = $sended_message;
        /*$words = StopWord::all();
        foreach ($words as $word) {
            $replaced_word ='';
            for($i=0; $i<count($words); $i++)
                $replaced_word .= '*';
            $message= str_replace ( $word->word , $replaced_word , $message );
        }*/
        $words = StopWord::all();
        $arrayOfWords = array();
        foreach ($words as $word) {
            $arrayOfWords[] = $word->word;
        }
        $message= str_ireplace ( $arrayOfWords , '*******' , $message );


        return $message;
    }
}
