<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;
use App\User;

class ProfileController extends Controller
{
    public function index()
    {
        return view('auth.profile');
    }

    public function addPic(Request $request)
    {
        if($request->hasFile('picture'))
            if($request->file('picture')->isValid())
            {
                $file=$request->file('picture');
                $extension = $file->extension();
                $file_name=Auth::user()->name.Auth::user()->id.'.'.$extension;
                $r1=$r2=rand ( 1, 99 );
                $path=public_path() ."/images/".$r1.'/'.$r2."/";
                $picture="/images/".$r1.'/'.$r2."/".$file_name;
                $file->move($path, $file_name);

                $user=User::all()->find(Auth::user()->id);
                $user->picture = $picture;
                $user->save();
            }


        return redirect('/profile');
    }

}
