@extends('layouts.app')

@section('content')
    <div class="content">
        <div class="title m-b-md">
            GuestBook
        </div>

        <div class="links">
            <a href="{{ route('guest_book') }}">Гостевая книга</a>
            <a href="{{ route('admin') }}">Админка</a>
        </div>
    </div>
@endsection
