@extends('layouts.app')

@section('content')
    <div class="container">
        <script src="{{ url('/js/records_customization.js') }}" async></script>
        <h1>Админка</h1>
        <div class="menu">
            <p><a href="#records">Записи</a></p>
            <p><a href="#stop-words">Список стоп слов</a></p>
        </div>

        <h2 id="records">Записи</h2>
        @foreach($records as $record)
            <div class="row record">

                <div class="col-md-2"><img align="right" class="rounded-circle user-pic" width="70px" height="70px" src="{{ url("$record->user_pic") }}"></div>
                <div class="message col-md-6">
                    <p><b>{{$record->user_name}}</b></p>
                    <p><span class="date">{{$record->created_at}}</span></p>
                    @if (strlen($record->f_message) < 200)
                        <p>{{$record->f_message}}</p>
                    @else
                        <p id="hidden_text">{{mb_substr($record->f_message, 0, 200)}}<a id="{{$record->f_message}}" href="" class="show_text">... Показать&nbsp;полностью</a></p>
                    @endif
                </div>
                <div class="col-md-4" align="right">
                    <div>
                        <form action="{{ route('edit_message') }}" method="post">
                            {{ csrf_field() }}
                            <input type="hidden" name="id" value="{{$record->id}}">
                            <input type="hidden" name="fromAdminPage" value="admin">
                            <input type="submit" class="btn btn-outline-warning upd" title="Редактировать запись" value="o">
                        </form>
                    </div>
                    <div>
                        <form action="{{ route('delete_message') }}" method="post">
                            {!! method_field('delete') !!}
                            {{ csrf_field() }}
                            <input type="hidden" name="id" value="{{$record->id}}">
                            <input type="hidden" name="fromAdminPage" value="admin">
                            <input type="submit" class="btn btn-outline-danger del" title="Удалить запись"  value="x">
                        </form>
                    </div>
                </div>
            </div>
        @endforeach
        {{ $records->links() }}



        <h2 id="stop-words">Список стоп слов</h2>
        <form class="add-word" action="{{ route('add_stop_word') }}" method="post">
            {{ csrf_field() }}
            <input type="text" size="20" name="word" required>
            <input type="submit" class="btn btn-success" value="Добавить стоп слово">
        </form>
        @foreach($words as $word)
            <div class="row">
            <p class="col-md-2">{{$word->word}}</p>
            <div class="col-md-2">
                <form action="{{ route('deletestop_word') }}" method="post">
                    {!! method_field('delete') !!}
                    {{ csrf_field() }}
                    <input type="hidden" name="id" value="{{$word->id}}">
                    <input type="submit" class="btn" title="Удалить слово"  value="x">
                </form>
            </div>
            </div>
        @endforeach




    </div>
@endsection
