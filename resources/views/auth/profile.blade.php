@extends('layouts.app')

@section('content')
    <div class="container">
        <h1>Профиль пользователя</h1>
        <div clsss="row">
            <div class="col-md-2  user-image"><img align="left" class="rounded-circle" width="200px" height="200px" src="{{ url(Auth::user()->picture ) }}"></div>
            <div class="col-md-6" align="left" >
                <p>Пользователь: {{Auth::user()->name}}</p>
                <form action="{{ route('addPic') }}" method="post"  enctype="multipart/form-data">
                    {{ csrf_field() }}
                    <p class="date">Вы можете заменить изображение
                        <input type="file" accept="image/jpeg,image/png" name="picture" required></p>
                    <input type="submit" class="btn btn-success" value="Обновить изображение">
                </form>
            </div>
        </div>
    </div>
@endsection
