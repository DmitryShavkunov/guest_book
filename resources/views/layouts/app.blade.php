<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>Guest book</title>

    <!-- Scripts -->
    <script src="{{ asset('js/app.js') }}" defer></script>

    <!-- Fonts -->
    <link rel="dns-prefetch" href="https://fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet" type="text/css">
    <link href="https://fonts.googleapis.com/css?family=Fira+Sans" rel="stylesheet">
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
    <script src="https://code.jquery.com/jquery-3.3.1.min.js"></script>
    <!-- Styles -->
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
    <style>
        body{
            font-family: 'Fira Sans', sans-serif;
        }

        .message > p{
            margin-bottom:5px;
        }

        .record{
            width:90%;
            border:1px solid lightgrey;
            border-radius: 10px;
            margin-bottom:10px;
            padding:15px;
            background-color: white;
        }

        .del,.upd{
            width:35px;
        }

        .date{
            color:gray;
        }

        .this-user{
            background-color:lightyellow;
        }

        #send-message{
            font-size:20px;
        }
        .send-message{
            margin:10px;
            padding:10px;
        }
        .form-send-message {
            border:1px solid lightgrey;
            border-radius: 10px;
            padding: 10px;
        }

        .menu {
            position: fixed; /* Фиксированное положение */
            left: 10px; /* Расстояние от правого края окна браузера */
            top: 20%; /* Расстояние сверху */
            padding: 10px; /* Поля вокруг текста */
        }

        .add-word{
            margin:10px;
            margin-left:0px;
            pading:10px;
            pading-left:0px;
        }
        .user-pic{
            border:1px solid lightblue;
        }

        .content {
            text-align: center;
        }

        .title {
            font-size: 84px;
        }

        .links > a {
            color: #636b6f;
            padding: 0 25px;
            font-size: 12px;
            font-weight: 600;
            letter-spacing: .1rem;
            text-decoration: none;
            text-transform: uppercase;
        }

        .m-b-md {
            margin-bottom: 30px;
        }

    </style>
</head>
<body>
    <div id="app">

        @include('layouts.nav');


        <main class="py-4">
            @yield('content')
        </main>
    </div>
</body>
</html>
