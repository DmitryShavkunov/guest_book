@extends('layouts.app')

@section('content')
    <script src="{{ url('/js/records_customization.js') }}" async></script>
    <div class="container">
        <h1>Гостевая книга</h1>
        <a id="send-message" href="#">Добавить отзыв</a>
        <div class="input-group send-message"  hidden>
            <form class="form-send-message" id="send_message" action="{{ route('send_message') }}" method="post">
                {{ csrf_field() }}
                <dl><dt></dt><dd><textarea rows="5" cols="100" name="message" required></textarea></dd></dl>
                <dl><dt></dt><dd> <input type="submit" class="btn btn-success" value="Отправить"></dd></dl>
            </form>
        </div>
        @foreach($records as $record)
            <div class="row record">

                <div class="col-md-2"><img align="right" class="rounded-circle user-pic" width="70px" height="70px" src="{{ url("$record->user_pic") }}"></div>
                <div class="message col-md-6">
                    <p><b>{{$record->user_name}}</b></p>
                    <p><span class="date">{{$record->created_at}}</span></p>
                    @if (strlen($record->f_message) < 200)
                        <p>{{$record->f_message}}</p>
                    @else
                        <p id="hidden_text">{{mb_substr($record->f_message, 0, 200)}}<a id="{{$record->f_message}}" href="" class="show_text">... Показать&nbsp;полностью</a></p>
                    @endif
                </div>
                <div class="col-md-4" align="right">
                    @if(($record->user_id == $user->id))
                    <div class="update">
                        <form action="{{ route('edit_message') }}" method="post">
                            {{ csrf_field() }}
                            <input type="hidden" name="id" value="{{$record->id}}">
                            <input type="submit" class="btn btn-outline-warning upd" title="Редактировать запись" value="o">
                        </form>
                    </div>
                    <div class="delete">
                        <form action="{{ route('delete_message') }}" method="post">
                                {!! method_field('delete') !!}
                                {{ csrf_field() }}
                                <input type="hidden" name="id" value="{{$record->id}}">
                                <input type="submit" class="btn btn-outline-danger del" title="Удалить запись"  value="x">
                        </form>
                    </div>
                    @endif
                </div>
            </div>
        @endforeach
        {{ $records->links() }}



    </div>
@endsection
