@extends('layouts.app')

@section('content')
<div class="container">
    <h1>Обновить запись</h1>

    <form action="{{ route('update_message') }}" method="post">
        {{ method_field('PUT') }}
        {{ csrf_field() }}
        <input type="hidden" name="id" value="{{$record->id}}" >
        <input type="hidden" name="fromAdminPage" value={{$isAdmin}}>
        <textarea rows="5" cols="45" name="message" required>{{$record->message}}</textarea>
        <input type="submit" value="Обновить запись">
    </form>


</div>
@endsection
